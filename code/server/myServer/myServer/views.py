from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .models import Strings
from .serializers import StringsSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def reverse(request):
    received_string = (request.GET.get('string', ''))
    print(received_string)
    new_string = reverse_string(received_string)
    return Response(StringsSerializer(new_string).data, status=status.HTTP_200_OK)


def reverse_string(x):
    new_string = Strings(original_string=x, reversed_string=x[::-1])
    new_string.save()

    return new_string
