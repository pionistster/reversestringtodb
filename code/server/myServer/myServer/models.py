from django.db import models


class Strings(models.Model):
    id = models.AutoField(primary_key=True)
    original_string = models.CharField(max_length=30, unique=True)
    reversed_string = models.CharField(max_length=30, unique=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s, %s' % (self.original_string, self.reversed_string)


