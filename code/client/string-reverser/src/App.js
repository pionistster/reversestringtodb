import './App.css';
import {useState} from "react";
import service from "./api";

function App() {
  const [string, setString] = useState("");
  const [reversedString, setReversedString] = useState("");

  const reverseStringOnClick = () =>{
      service.StringsService.getReversedString(string).then(response =>{
          setReversedString(response.reversed_string)
      })
  }

  return(
      <div className={'app'}>
        <input placeholder={"Enter String"} onChange={event => setString(event.target.value)}/>
        <output>{reversedString}</output>
        <button onClick={reverseStringOnClick}>Reverse</button>
      </div>
  )
}

export default App;
